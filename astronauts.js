"use strict";

const { ApolloServer, gql} = require('apollo-server');
const { buildSubgraphSchema } = require("@apollo/federation");

const port = 4001;

const astronauts = [
    {
        id: 1,
        name: "Mansoor Jafary"
    },
    {
        id: 2,
        name: "Mikail Jafary"
    },
    {
        id: 3,
        name: "Alisha Jafary"
    },
    {
        id: 4,
        name: "Amna Jafary"
    }
]

const typeDefs = gql`
    type Astronaut @key(fields: "id") {
        id: ID!
        name: String
    }

    extend type Query {
        astronaut(id: ID!): Astronaut
        astronauts: [Astronaut]
    }
`;

const resolvers = {
    Astronaut: {
        __resolveReference(ref) {
            console.log('Astronaut Resolve Reference:', ref)
            const astronaut = astronauts.find(astronaut => astronaut.id == ref.id);
            console.log('Astronaut:', astronaut);
            return astronaut;
        }
    },  
    Query: {
        astronaut(_, args) {
            console.log(args);
            const { id } = args;
            return astronauts.find(id => id === id);
        },
        astronauts() {
            return astronauts;
        }
    }
}

const server = new ApolloServer({
    schema: buildSubgraphSchema([{ typeDefs, resolvers }])
});

server.listen({ port }).then(({ url }) => {
    console.log(`Astronaut Server ready at ${ url }`)
});