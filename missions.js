"use strict";

const { ApolloServer, gql} = require('apollo-server');
const { buildSubgraphSchema } = require("@apollo/federation");

const port = 4002;

const missions = [
    {
        id: 1,
        designation: "Mission1",
        startDate: "2021-01-01",
        endDate: "2021-02-01",
        crew: [1, 2]
    },
    {
        id: 2,
        designation: "Mission2",
        startDate: "2021-02-01",
        endDate: "2021-03-01",
        crew: [2, 3]

    },
    {
        id: 3,
        designation: "Mission3",
        startDate: "2021-03-01",
        endDate: "2021-04-01",
        crew: [3, 4]
    },
]

const typeDefs = gql`
    type Mission @key(fields: "id") {
        id: ID!
        designation: String!
        startDate: String
        endDate: String
        crew: [Astronaut]
    }
    extend type Astronaut @key(fields: "id") {
        id: ID! @external
        missions: [Mission]
    }

    extend type Query {
        mission(id: ID!): Mission
        missions: [Mission]
    }
`;

const resolvers = {
    Astronaut: {
        missions(astronaut) {
            return missions.filter(mission => mission.crew.includes(astronaut.id));
        }
    },
    Mission: {
        crew(mission) {
            console.log(mission);
            const crewIds = mission.crew.map(id => ({ id }));
            console.log('Crew IDs:', crewIds);
            return crewIds;
        }
    },
    Query: {
        mission(_, { id }) {
            return missions.find(id => id === id);
        },
        missions() {
            return missions;
        }
    }
}

const server = new ApolloServer({
    schema: buildSubgraphSchema([{ typeDefs, resolvers }])
});

server.listen({ port }).then(({ url }) => {
    console.log(`Missions Server ready at ${ url }`)
});