# Includes  

- Apollo Gateway  - running at http://localhost:4000  
- Apollo Missions GrahQL API  - running at http://localhost:4001  
- Apollo Astronauts API  - running at http://localhost:4002  

# How to Run  
```npm install ```  
``` npm start ```  

# Sample Query  
```
{
  missions {
    id
    designation
    startDate
    endDate
    crew {
      id
      name
      missions {
        id
      }
    }
  }
}
```  

# Credits (with thanks!)  
Mindy Wise - https://www.youtube.com/watch?v=v_1bn2sHdk4  
